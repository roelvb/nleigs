NLEIGS Matlab toolbox
=====================

- Authors: Roel Van Beeumen and Stefan Guettel
- Date:    April 5, 2016
- Version: 0.5


Main file
---------

- nleigs.m: find a few eigenvalues and eigenvectors of a nonlinear eigenvalue problem.

Instructions can be found in the header of the file.


Auxiliary files
---------------

- lejabagby.m
- ratnewtoncoeffs.m
- ratnewtoncoeffsm.m
- discretizepolygon.m
- evalrat.m
- nleigsplot.m
- plot\_conv_eig.m

Instructions can be found in the header of the files.


Examples
--------

The examples folder contains the numerical experiments for the 'gun' and the 'particle in a canyon' problem. For running the 'gun' problem, the user should first install the [NLEVP collection](http://www.mims.manchester.ac.uk/research/numerical-analysis/nlevp.html).


- gun_variantP.m
- gun_variantR1.m
- gun_variantR2.m
- gun_variantS.m
- particle_variantR2.m
- particle_variantS.m


Paper
-----

S. Guettel, R. Van Beeumen, K. Meerbergen, and W. Michiels  
_NLEIGS: A class of fully rational Krylov methods for nonlinear eigenvalue problems_  
SIAM Journal on Scientific Computing, 36 (6), A2842-A2864, 2014.

All scientific publications, for which the software has been used, must mention the use of the software, and must refer to the above publication.


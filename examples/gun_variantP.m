% gun_variantP

addpath ../

clear; clc; display('>> gun_variantP');
m = 100;
timings = 0;
solver = @nleigs;

%% nlep formulation
NLEP = gun_init;
gam = 300^2 - 200^2; mu = 250^2; sigma2 = 108.8774;
xmin = gam*(-1) + mu;
xmax = gam*1 + mu;

% define target set Sigma
npts = 1000;
line = linspace(xmin,xmax,npts);
halfcircle = xmin+(xmax-xmin)*(exp(1i*linspace(0,pi,round(pi/2*npts)+2))/2+.5);
Sigma = [ halfcircle, xmin ];

% sequence of interpolation nodes
Z = [2/3,(1+1i)/3,0,(-1+1i)/3,-2/3].';
nodes = gam*Z + mu;

% define the set of pole candidates
Xi = [];

% options
maxit = m;
randn('state',0);
v0 = randn(9956,1);
F = {@(x)1;@(x)x;@(x)1i*sqrt(x);@(x)1i*sqrt(x-sigma2^2)};
BC = [NLEP.B{1};NLEP.B{2};NLEP.C{1};NLEP.C{2}];
funres = @(Lam,X) gun_residual(Lam,X,BC,F);

%% polynomial case (only repeated nodes)
if timings
    disp = 0;
else
    disp = 1;
end
leja = 0;
reuselu = 2;
options = struct('disp',disp,'maxit',maxit,'v0',v0,'funres',funres,...
   'leja',leja,'nodes',nodes,'reuselu',reuselu);

% solve nlep
if timings
    tic;
    lambda = solver(NLEP,Sigma,Xi,options);
    toc;
else
    [X,lambda,res,info] = solver(NLEP,Sigma,Xi,options);

    %% plot results
    [fig,ResIn,ResOut] = gun_nleigsplot(lambda,info,Sigma,'b-','k:','b-');
    
    figure(fig(3));
    title('Gun: variant P');
    
    figure(fig(2));
    title('Gun: variant P');
    xlim([0,m]); ylim([1e-17,1e-1]);
    
    figure(fig(1));
    title('Gun: variant P');
    axis equal; xlim([0,350]); ylim([0,150]);
    
end

% particle_variantS

addpath ../

clear; clc; display('>> particle_variantS');
nbit = 200;
interval = 2;
timings = 0;
solver = @nleigs;

% nlep
[A,brpts] = particle_init(interval);
n = size(A.B{1},1);

% interval
sep = 1e-4;
if interval == 1
    xmin = -U0;
    xmax = brpts(interval) - sep;
    Xi = logspace(-6,6,1e4) + brpts(interval);
elseif interval > 1
    xmin = brpts(interval-1) + sep;
    xmax = brpts(interval) - sep;
    Xi1 = -logspace(-6,6,5e3) + brpts(interval-1);
    Xi2 = logspace(-6,6,5e3) + brpts(interval);
    Xi = [Xi1,Xi2];
else
    error('@rvb: wrong interval!')
end

% define target set Sigma
Sigma = [ xmin , xmax ];

% options
maxit = nbit;
randn('state',0);
v0 = randn(n,1);
nodes = linspace(xmin,xmax,11);
nodes = nodes(2:2:end);
funres = @(Lam,X) particle_residual(Lam,X,A);

%% variant S
if timings
    disp = 0;
else
    disp = 1;
end
minit = 120;
maxdgr = 50;
static = true;
options = struct('disp',disp,'maxdgr',maxdgr,'minit',minit,'maxit',maxit,...
    'v0',v0,'funres',funres,'nodes',nodes,'static',static);

% solve nlep
if timings
    tic;
    lambda = solver(A,Sigma,Xi,options);
    toc;
else
    [X,lambda,res,info] = solver(A,Sigma,Xi,options);
    
    %% plot results
    approxSigma = [xmin-1i*1e-10, xmin+1i*1e-10, xmax+1i*1e-10, xmax-1i*1e-10];
    [fig,ResIn,ResOut] = nleigsplot(lambda,info,approxSigma,'r-','k:','r-');
    
    figure(fig(3));
    title('Particle: variant S');
    
    figure(fig(2));
    title('Particle: variant S');
    
    figure(fig(1));
    title('Particle: variant S');
    plot(real(nodes),imag(nodes),'om');
    xlim([xmin-(xmax-xmin)/2,xmax+(xmax-xmin)/2]); ylim([-1,1]);
    
end

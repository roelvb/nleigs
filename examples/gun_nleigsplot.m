function [fig,ResIn,ResOut,LamIn,LamOut] = ...
    gun_nleigsplot(lambda,info,Sigma,varargin)
%GUN_NLEIGSPLOT  Plot results of NLEIGS for the gun problem
%
%   [fig,ResIn,ResOut,LamIn,LamOut] = GUN_NLEIGSPLOT(lambda,info,Sigma)
%
%   See also NLEIGS, NLEIGSPLOT.
%
%   version: 28-09-2015

if nargin >= 6, plotopts3 = varargin{3};
else plotopts3 = '-k'; end
if nargin >= 5, plotopts2 = varargin{2};
else plotopts2 = ':k'; end
if nargin >= 4, plotopts1 = varargin{1};
else plotopts1 = '-k'; end

%% data
sqrtlam = sqrt(lambda);
sqrtSigma = sqrt(Sigma);
nodes = sqrt(info.sigma(1:length(info.nrmD)));
shifts = sqrt(unique(info.sigma(length(info.nrmD)+1:end)));
xi = sqrt(info.xi(info.xi>0));

%% plot eigenvalues
fig(1) = figure;
fill(real(sqrtSigma),imag(sqrtSigma),[0.95 0.95 0.95]); hold on;
plot(real(nodes),imag(nodes),'gx','MarkerSize',5);
plot(real(xi),zeros(size(xi)),'r.');
plot(real(shifts),imag(shifts),'mo');
plot(real(sqrtlam),imag(sqrtlam),'b*');
xlabel('Re($\sqrt{\lambda}$)','Interpreter','latex');
ylabel('Im($\sqrt{\lambda}$)','Interpreter','latex');
legendtext{1} = 'target set $\Sigma$';
legendtext{2} = 'nodes $\sigma$';
legendtext{3} = 'poles $\xi$';
if ~isempty(shifts)
    legendtext{end+1} = 'shifts';
end
if ~isempty(lambda)
    legendtext{end+1} = 'eigenvalues $\lambda$';
end
legend(legendtext(:),'Location','NorthWest','Interpreter','latex');

%% convergence plot
fig(2) = figure;
[ResIn,ResOut,LamIn,LamOut] = plot_conv_eig(info.Lam,info.Res,Sigma,...
    plotopts1,plotopts2);

%% approximation plot
fig(3) = figure;
semilogy(0:length(info.nrmD)-1,info.nrmD,plotopts3);
title('Error estimation of expansion');
xlabel('iteration $j$','Interpreter','latex');
ylabel('$\delta_j$','Interpreter','latex');

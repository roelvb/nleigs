function NLEP = gun_init

coeffs = nlevp('gun');
sigma2 = 108.8774;

%% polynomial matrices
B{1} = coeffs{1}; % K
B{2} = -coeffs{2}; % M

%% nonlinear matrices
C{1} = coeffs{3}; % W1
C{2} = coeffs{4}; % W2

%% compute svd decomposition of C{1}
W1 = C{1}(9722:end,9722:end);
[L1,U1] = lu(W1);
[L1,U1] = compactlu(L1,U1);
L{1} = spalloc(9956,size(L1,2),nnz(L1));
L{1}(9722:end,:) = L1;
U{1} = spalloc(size(U1,1),9956,nnz(U1));
U{1}(:,9722:end) = U1;
U{1} = U{1}';

%% compute svd decomposition of C{2}
W2 = C{2}(1:479,1:479);
[L2,U2] = lu(W2);
[L2,U2] = compactlu(L2,U2);
L{2} = spalloc(9956,size(L2,2),nnz(L2));
L{2}(1:479,:) = L2;
U{2} = spalloc(size(U2,1),9956,nnz(U2));
U{2}(:,1:479) = U2;
U{2} = U{2}';

%% nonlinear functions
f{1} = @(x) 1i*sqrtm(x);
f{2} = @(x) 1i*sqrtm(x - sigma2^2*x^0);

%% nlep
NLEP = struct('B',{B},'C',{C},'L',{L},'U',{U},'f',{f});

end

function [Lc,Uc] = compactlu(L,U)

n = size(L,1);
select = false(n,1);
for i = 1:n
    l = L(i:n,i);
    u = U(i,i:n);
    if nnz(l) > 1 || nnz(u) > 0
        select(i) = true;
    end
end

Lc = L(:,select);
Uc = U(select,:);

end

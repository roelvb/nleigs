function R = gun_residual(Lambda,X,A,F)

% constants
sigma1 = 0;
sigma2 = 108.8774;

nK = 1.474544889815002e+05;   % norm(K,1)
nM = 2.726114618171165e-02;   % norm(M,1)
nW1 = 2.328612251920476e+00;  % norm(W1,1)
nW2 = 3.793375498194695e+00;  % norm(W2,1)

% initialization
[N,n] = size(A);
m = N/n;
k = length(Lambda);

% Denominator
Den = nK + abs(Lambda)*nM + sqrt(abs(Lambda-sigma1^2))*nW1 + sqrt(abs(Lambda-sigma2^2))*nW2;

% 2-norm of A(lambda)*x
AX = reshape(A*X,[],m,k);
FL = cell2mat(cellfun(@(x) arrayfun(x,Lambda),F(:)','UniformOutput',0));
RR = sum(bsxfun(@times,AX,reshape(FL.',1,size(AX,2),[])),2);
R = zeros(k,1);
for i = 1:k
	R(i) = norm(RR(:,:,i))/Den(i);
end

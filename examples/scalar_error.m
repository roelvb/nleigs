% scalar_error
% 
% Compare convergence of polynomial and rational interpolation for a
% scalar function on the interval [0.01,4].

addpath ../

clear; clc; display('>> scalar_error');

A = @(X) .2*(sqrt(X) - 3*sin(2*X));
Am = @(X) .2*(sqrtm(X) - 3*funm(2*X,@sin)); % our nonlinear operator

m = 100; % maximal order of interpolant, type = [m,m] or [m,0]
a = 0.01; 
S1 = discretizepolygon([a,4],1e4); % region of interest
S2 = -logspace(-4,4,1e4); % discretization of singularity set
S3 = logspace(log10(a),log10(4),1e3); % control set

xx = S3;
exact = A(xx);


%% Chebyshev interpolation
N = 200;
t = a + (4-a)*(cos( pi*(0:N)/N ) + 1)/2;
gt = A(t);
c = [ gt , gt(N:-1:2) ];
c = real(fft(c))/N;
c(1) = c(1)/2;
c = c(1:m+1);
xxx = 2*(xx-a)/(4-a)-1;
p = 0*xxx;
err_cheb = zeros(m,1);
for j = 1:m
    p = p + c(j)*cos((j-1)*acos(xxx));
    err_cheb(j) = norm(exact - p,inf); 
end


%% rational Newton interpolation
[sigma,xi,beta] = lejabagby(S1,S2,S3,m+1); % compute nodes and poles
xi(m+1) = NaN; % not used
D = ratnewtoncoeffs(A,sigma,xi,beta);

% check uniform error of rational interpolant on S1
r = 0;
nrm_rat = zeros(m+1,1);
err_rat = zeros(m+1,1);
for j = 1:m+1,
    erat = evalrat(sigma(1:j-1),xi(1:j-1),beta(1:j),xx);
    r = r + D{j}*erat;
    nrm_rat(j) = norm(erat,inf);
    err_rat(j) = norm(exact - r,inf);% / norm(exact,inf);    
end

% convergence rate
kappa = 4/a;
prate = (sqrt(kappa)-1)/(sqrt(kappa)+1);
x_prate = (40:m)';
y_prate = 6e-2*prate.^x_prate;


%% polynomial case
S2 = ones(1,m+1)*inf;
[sigma,xi,beta] = lejabagby(S1,S2,S3,m+1); % compute nodes and poles
xi(m+1) = NaN;
D = ratnewtoncoeffs(A,sigma,xi,beta);

% check uniform error of polynomial interpolant on S1
r = 0;
nrm_poly = zeros(m+1,1);
err_poly = zeros(m+1,1);
for j = 1:m+1,
    erat = evalrat(sigma(1:j-1),xi(1:j-1),beta(1:j),xx);
    r = r + D{j}*erat;
    nrm_poly(j) = norm(erat,inf);
    err_poly(j) = norm(exact - r,inf);% / norm(exact,inf);
end

% convergence rate
Imin = a; Imax = 4;
delta = sqrt(Imin/Imax);
mu = ( (1-delta)/(1+delta) );
myellip = @(kappa) ellipke(kappa^2);
pred2 = exp(-pi/4*myellip(sqrt(1-mu^2))/myellip(mu)); 
x_rrate = (40:75)';
y_rrate = 1e4*pred2.^x_rrate;


%% plot interpolation error
figure;
semilogy(err_cheb,'c-.'); hold on
semilogy(err_poly,'b--');
semilogy(x_prate,y_prate,'b:');
semilogy(err_rat,'r-');
semilogy(x_rrate,y_rrate,'r:');
legend('polyn. Chebyshev','polyn. Leja','rate (1.3)','rat. Leja-Bagby',...
    'rate (1.4)','Location','SouthWest');
xlabel('number of interpolation nodes');
ylabel('uniform error');
title('interpolation error');
grid on 
axis([0,m,1e-16,1e1]);

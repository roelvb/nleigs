function R = particle_residual(Lambda,X,NLEP)

k = length(Lambda);
R = zeros(k,1);

Lam = zeros(k,1);
for i = 1:k
    Lam(i) = real(Lambda(i)) + 1i*(sign(imag(Lambda(i)))*imag(Lambda(i)));
end

for i = 1:k
    R(i) = norm(funA(Lam(i),X(:,i)));
end

    function A = funA(lam,x)
        
        A = NLEP.B{1}*x;
        for j = 2:length(NLEP.B)
            A = A + lam^(j-1)*(NLEP.B{j}*x);
        end
        for j = 1:length(NLEP.C)
            A = A + NLEP.f{j}(lam)*(NLEP.C{j}*x);
        end
        
    end

end

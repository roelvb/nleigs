% scalar_solve

addpath ../

clear; clc; display('>> scalar_solve');

m = 100;

xmin = 0.01;
xmax = 4;

% polynomial matrices
B = {};

% nonlinear matrices
C{1} = 0.2;
C{2} = 0.2*(-3);

% nonlinear functions
f{1} = @(x) sqrt(x);
f{2} = @(x) sin(2*(x));

% nlep
A = struct('B',{B},'C',{C},'f',{f});

% define target set Sigma
Sigma = [xmin,xmax];

% options
options = struct('maxit',m,'leja',2,'tol',100*eps,'isfunm',false);


%% polynomial case
% solve nlep
[Xp,lambdap,resp,infop] = nleigs(A,Sigma,[],options);

%% fully rational case
% define set of poles candidates
Xi = -logspace(-6,5,1e4);
% solve nlep
[Xr,lambdar,resr,infor] = nleigs(A,Sigma,Xi,options);


%% plot residuals
figure;
approxSigma = [xmin-1i*1e-8, xmin+1i*1e-8, xmax+1i*1e-8, xmax-1i*1e-8];
plot_conv_eig(infop.Lam,infop.Res,approxSigma,'b--','');
plot_conv_eig(infor.Lam,infor.Res,approxSigma,'r-','');
title('convergence of residuals')
xlabel('iteration')
ylabel('2-norm of residual A(\lambda)x')
axis([0,m,1e-16,1e1])
grid on

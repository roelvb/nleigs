% gun_variantR2

addpath ../

clear; clc; display('>> gun_variantR2');
m = 100;
timings = 0;
solver = @nleigs;

%% nlep formulation
NLEP = gun_init;
gam = 300^2 - 200^2; mu = 250^2; sigma2 = 108.8774;
xmin = gam*(-1) + mu;
xmax = gam*1 + mu;

% define target set Sigma
npts = 1000;
line = linspace(xmin,xmax,npts);
halfcircle = xmin+(xmax-xmin)*(exp(1i*linspace(0,pi,round(pi/2*npts)+2))/2+.5);
Sigma = [ halfcircle, xmin ];

% sequence of interpolation nodes
Z = [2/3,(1+1i)/3,0,(-1+1i)/3,-2/3].';
nodes = gam*Z + mu;

% define the set of pole candidates
Xi = -logspace(-8,8,1e4) + sigma2^2;

% options
maxit = m;
randn('state',0);
v0 = randn(9956,1);
F = {@(x)1;@(x)x;@(x)1i*sqrt(x);@(x)1i*sqrt(x-sigma2^2)};
BC = [NLEP.B{1};NLEP.B{2};NLEP.C{1};NLEP.C{2}];
funres = @(Lam,X) gun_residual(Lam,X,BC,F);

%% fully rational case (leja + repeated nodes + freeze)
if timings
    disp = 0;
else
    disp = 1;
end
minit = 60;
options = struct('disp',disp,'minit',minit,'maxit',maxit,'v0',v0,...
    'funres',funres,'nodes',nodes);

% solve nlep
if timings
    tic;
    lambda = solver(NLEP,Sigma,Xi,options);
    toc;
else
    [X,lambda,res,info] = solver(NLEP,Sigma,Xi,options);
    
    %% plot results
    [fig,ResIn,ResOut] = gun_nleigsplot(lambda,info,Sigma,'r-','k:','r-.');
    
    figure(fig(3));
    title('Gun: variant R2'); xlim([0,m]);
    
    figure(fig(2));
    title('Gun: variant R2');
    xlim([0,m]); ylim([1e-17,1e-1]);
    
    figure(fig(1));
    title('Gun: variant R2');
    axis equal; xlim([0,350]); ylim([0,150]);
    
end

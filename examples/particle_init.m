function [NLEP,brpts] = particle_init(interval)

% author: William Vandenberghe

%% initialization
% constants
meter = 1/5.2917725e-11;
nm = 1e-9*meter;
eV = 1/13.6;

% parameters
xmax = 5;
zmax = 2;

xstep = 0.05;
zstep = 0.05;

x_x = (-xmax:xstep:xmax)*nm;
z_z = (-zmax:zstep:zmax)*nm;

nx = length(x_x);
nz = length(z_z);

dx = min(diff(x_x));
dz = min(diff(z_z));

x = kron(x_x,z_z.^0);
z = kron(x_x.^0,z_z);

w1 = 1*nm;
w2 = 1.1*nm;
l = 4*nm;
U0 = 3*eV;

% potential U
U = 0*x;
U(abs(z)<w1) = -U0;
U(abs(z)<w2 & abs(x)<l/2) = -U0;

% m
m = 0.2;

% branch points
n = nx*nz;
Dxx_x = spdiags(ones(nx,1)*[1 -2 1]/dx^2,-1:1,nx,nx);
Dzz_z = spdiags(ones(nx,1)*[1 -2 1]/dz^2,-1:1,nz,nz);
H_L = -1/m*Dzz_z + diag(U(1:nz));
H_R = -1/m*Dzz_z + diag(U(end-nz+(1:nz)));

if norm(H_L - H_R) == 0
    % symmetric potential
    [V,D] = eig(H_L);
    [d,i] = sort(diag(D));
    V = V(:,i);
    p = zeros(nz,1); % 0 (left+right)
else
    % asymmetric potential
    [V_L,D_L] = eig(H_L);
    [V_R,D_R] = eig(H_R);
    V = [V_L,V_R];
    p = [-ones(nz,1);ones(nz,1)];
    [d,i] = sort([diag(D_L);diag(D_R)]);
    V = V(:,i);
    p = p(i); % -1 (left), 1 (right)
end

%% matrix H
H = -1/m*(kron(Dxx_x,speye(nz))+kron(speye(nx),Dzz_z)) + spdiags(U',0,n,n);

%% branch points
brpts = unique(d);

%% polynomial matrices
B{1} = H;
B{2} = -speye(n);

%% nonlinear matrices
SL = cell(length(brpts),1);
if p(1) < 0
    SL{1} = [V(:,1);sparse(n-nz,1)];
elseif p(1) == 0
    SL{1} = [ [V(:,1);sparse(n-nz,1)], [sparse(n-nz,1);V(:,1)] ];
else
    SL{1} = [sparse(n-nz,1);V(:,1)];
end
c = 1;
for j = 2:length(d)
    if d(j-1) == d(j)
        if p(j) < 0
            SL{c} = [ SL{c}, [V(:,j);sparse(n-nz,1)] ];
        elseif p(j) == 0
            SL{c} = [ SL{c}, [V(:,j);sparse(n-nz,1)], [sparse(n-nz,1);V(:,j)] ];
        else
            SL{c} = [ SL{c}, [sparse(n-nz,1);V(:,j)] ];
        end
    else
        c = c + 1;
        if p(j) < 0
            SL{c} = [V(:,j);sparse(n-nz,1)];
        elseif p(j) == 0
            SL{c} = [ [V(:,j);sparse(n-nz,1)], [sparse(n-nz,1);V(:,j)] ];
        else
            SL{c} = [sparse(n-nz,1);V(:,j)];
        end
    end
end
SU = SL;
for j = 1:length(SL)
    SL{j} = -1/m/dx^2*SL{j};
end
S = cell(length(brpts),1);
for j = 1:length(S)
    S{j} = SL{j}*SU{j}';
end

%% nonlinear functions
f = cell(length(brpts),1);
for j = 1:interval-1
    %f{j} = @(lambda) -1/m/dx^2*expm(1i*sqrtm(m*(lambda-brpts(j)*lambda^0)));
    f{j} = @(lambda) expm(1i*sqrtm(m*(lambda-brpts(j)*lambda^0)));
end
for j = interval:length(brpts)
    %f{j} = @(lambda) -1/m/dx^2*expm(-sqrtm(m*(-lambda+brpts(j)*lambda^0)));
    f{j} = @(lambda) expm(-sqrtm(m*(-lambda+brpts(j)*lambda^0)));
end

%% nlep
NLEP = struct('B',{B},'C',{S},'L',{SL},'U',{SU},'f',{f});

end

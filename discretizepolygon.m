function [zz,Z] = discretizepolygon(z,npts,nsigma)
% ZZ = DISCRETIZEPOLYGON(Z)
% Discretize the polygon given by the (complex) entries of Z.

doneBoundary = 0;

if nargin < 3,
    nsigma = 5; % nr of interior points
end

if nargin < 2,
    npts = 1e4; % nr of boundary points
end

if nargin == 0 || isempty(z), % return unit disk
    z = 0;
end

if length(z) == 1, % return disk centered at z
    zz = z + exp(2i*pi*(1:npts)/npts);
    doneBoundary = 1;
end

if length(z) == 2, % return Chebyshev points
    zz = (z(2)-z(1))/2*(cos(pi*(npts-1:-1:0)/(npts-1))+1) + z(1);
    doneBoundary = 1;
end

if ~doneBoundary,
    z = [ z(:) ; z(1) ]; % close polygon
    L = sum(abs(diff(z))); % length of polygon
    
    % 0<=alph<=1 is position of current point between z(ind) and z(ind+1)
    ind = 1; alph = 0; zz = z(ind);
    remL = L/npts;
    while length(zz) < npts,
        d = abs(z(ind+1) - z(ind));
        if (1-alph)*d < remL, % can go to end of edge
            ind = ind + 1;
            remL = remL - (1-alph)*d;
            alph = 0;
        else % next point zz is between z(ind) and z(ind+1)
            alph = alph + remL/d;
            remL = L/npts;
            zz = [ zz , z(ind) + alph*(z(ind+1)-z(ind)) ];
        end
    end
end

zz = [ zz, z(:).' ];


if nargout > 1, % want interior points
    
    if length(z) == 2, % interval case
        xnr = 2*nsigma;
        if ~mod(xnr,2), xnr = xnr + 1; end
        xpts = linspace(z(1),z(2),xnr);
        Z = xpts(2:2:end);
        return
    end
    
    iter = 0;
    spacing = (max(real(z)) - min(real(z)))/2.0001/sqrt(nsigma); Z = [];
    while length(Z) < nsigma,
        
        iter = iter + 1;
        if iter > 10,
            error('DISCRETIZEPOLYGON: Failed to find interior polygon points. (Note that intervals should be given their two endpoints only.)');
        end
        
        xnr = (max(real(z))-min(real(z)))/(2*spacing);
        if ~mod(xnr,2), xnr = xnr + 1; end
        xpts = linspace(min(real(z)),max(real(z)),xnr);
        xpts = xpts(2:2:end);
        
        ynr = (max(imag(z))-min(imag(z)))/(2*spacing);
        if ~mod(ynr,2), ynr = ynr + 1; end
        ypts = linspace(min(imag(z))-eps,max(imag(z))+eps,ynr);
        ypts = ypts(2:2:end);
        
        [X,Y] = meshgrid(xpts,ypts);
        Z = X + 1i*Y; Z = Z(:);
        ind = inpolygon(real(Z),imag(Z),real(z),imag(z));
        Z = Z(ind);
        spacing = spacing / sqrt(sqrt(2));
        
    end
end

function D = ratnewtoncoeffsm(fm,sigma,xi,beta)
% Compute rational divided differences for the SCALAR function fun, using
% matrix functions. fm has to be a handle to a matrix function fm(A).

m = length(sigma)-1;

sigma = sigma(:);
xi = xi(:); xi(m+1) = NaN; % not used
beta = beta(:);

% build Hessenberg matrices
K = full(spdiags([ [beta(2:m+1)./xi(1:m);0] , ones(m+1,1) ],-1:0,m+1,m+1));
H = full(spdiags([ [beta(2:m+1);0] , sigma(1:m+1) ],-1:0,m+1,m+1));

%S = diag(1./beta);
%K = K*S;
%H = H*S;

if 1, % column balancing
    P = diag(1./max(abs(K),[],1));
    K = K*P;
    H = H*P;
end

D = fm(H/K)*eye(m+1,1)*beta(1);
D = D.';
D = num2cell(D);

end
